static variable Equation_Number = 0;
static define run_cmd (cmd)
{
   if (-1 == system (cmd))
     vmessage ("****WARNING: %s failed\n", cmd);
}

static define equation_function_env (esc_eq, env)
{
   esc_eq = strtrim (esc_eq);
   variable eq, len;
   len = strlen (esc_eq);
   if (0 == strncmp (esc_eq, "#v+", 3))
     {
	(esc_eq,) = strreplace (esc_eq, "#v+", "", len);
	(esc_eq,) = strreplace (esc_eq, "#v-", "", len);
	(esc_eq,) = strreplace (esc_eq, "#v", "", len);
	esc_eq = strtrim (esc_eq);
	eq = esc_eq;
     }
   else
     {
	(eq,) = strreplace (esc_eq, "\\\\", "\\", len);
	(eq,) = strreplace (eq, "&lt;", "<", len);
	(eq,) = strreplace (eq, "&gt;", ">", len);
	(eq,) = strreplace (eq, "&amp;", "&", len);
     }


   variable text = "\\documentclass[12pt]{article}\n";
   text += "\\usepackage{amsmath}\n";
   text += "\\begin{document}\n";
   text += "\\pagestyle{empty}\n";
   text += sprintf ("\\begin{%s}\n%s \\nonumber\n\\end{%s}", env, eq, env);
   text += "\n\\end{document}\n";
   
   Equation_Number++;
   variable tmpdir = "tmp";
   variable autopng = "autopng";

   variable base = sprintf ("eq%d", Equation_Number);
   () = mkdir (tmpdir, 0777);
   () = mkdir (autopng, 0777);
   variable tex = sprintf ("%s.tex", base);
   variable dvi = base + ".dvi";

   if (-1 == write_string_to_file (text, sprintf ("%s/%s", tmpdir, tex)))
     verror ("Write to %s failed\n", tex);
        
   run_cmd (sprintf ("cd %s; latex %s", tmpdir, tex));
   run_cmd (sprintf ("cd %s; dvi2png %s", tmpdir, dvi));
   variable png = sprintf ("%s/%s_eq%d.png", autopng, __TM_FILE__, Equation_Number);
   run_cmd (sprintf ("mv %s/%s.png %s", tmpdir, dvi, png));
   
   vinsert ("<img src=\"%s\" alt=\"%s\" border=0>\n", png, esc_eq);
}

public define equation_function (esc_eq)
{
   equation_function_env (esc_eq, "equation*");
}
public define eqnarray_function (esc_eq)
{
   equation_function_env (esc_eq, "eqnarray*");
}
tm_add_macro ("equation",&equation_function, 1, 1);
tm_add_macro ("eqnarray",&eqnarray_function, 1, 1);
