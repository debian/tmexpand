static define convert_color_to_theme (color)
{
   color = strtrim (color);
   if (color[0] != '#')
     verror ("convert_bgcolor_to_theme: %s must be specified in hex form",
	     color);
   color = strlow (color [[1:]]);
   insert (color);
}

tm_add_macro ("convert_color_to_theme", &convert_color_to_theme, 1, 1);

static define convert_rgb_to_hex (r, g, b)
{
   vinsert ("#%02X%02X%02X", integer(r), integer(g), integer(b));
}
tm_add_macro ("rgb", &convert_rgb_to_hex, 3, 3);

