#% Some common greek symbols
#d alpha \space&alpha;
#d beta \space&beta;
#d gamma \space&gamma;
#d delta \space&delta;
#d tau \space&tau;
#d Delta \space&Delta;

#% Some math "environments"
#s+
#i mathsym.sl
#s-
#d displayeq#1 \center{\equation{$1}}
#d displayeqnarray#1 \center{\eqnarray{$1}}
#d math#1 \em{$1}
