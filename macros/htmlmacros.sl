static define despamify_email (email)
{
   (email, ) = strreplace (email, ".", " ", strlen (email));
   (email, ) = strreplace (email, "@", " at ", strlen(email));

   insert (email);
}
tm_add_macro ("spamproof", &despamify_email, 1, 1);
