#% HTML Text-Macros
#i tmutil.tm
#% ---------------------------------------------------------------------------
#% Control Variables
#% ---------------------------------------------------------------------------
#d __verbatim_begin <pre>
#d __verbatim_end </pre>
#d __passthru_begin 
#d __passthru_end
#d __remove_empty_lines 1
#% ---------------------------------------------------------------------------
#% Macros
#% ---------------------------------------------------------------------------
#d html-start <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
#d html-end </HTML>
#% #d body <BODY bgcolor="#ffffff">
#d body#0:1 <BODY \optarg{$1}>
#d body-end </BODY>
#d underline#1 <u>$1</u>

#d ul#1 <ul>$1</ul>
#d olist <ol>
#d olist-end </ol>
#d ulist <ul>
#d ulist-end </ul>
#d item <li>
#d dl#1 <dl>$1</dl>
#d dt#1 <dt>$1</dt>
#d dd#1 <dd>$1</dd>
#d dtdd#2 <dt>$1</dt>\__newline__<dd>$2</dd>

#d title#1 <TITLE>$1</TITLE>
#d base#1 <BASE href="$1">
#d head#1 <HEAD>$1\__newline__\optarg{\meta-content-type} \
  \__newline__\optarg{\meta-keywords} \
  \__newline__\optarg{\meta-description}</HEAD>
#d p#0:1 <p \optarg{$1}>
#d par <p>
#d em#1 <em>$1</em>
#d kbd#1 <kbd>$1</kbd>
#d code#1 <code>$1</code>
#d samp#1 <samp>$1</samp>
#% d class_href#3 <A class="$1" HREF="$2">$3</A>
#d href#2:3 <A HREF="$1" \optarg{$3}>$2</A>
#d label#1 <A NAME="$1"></A>
#d url#1 <A HREF="$1">$1</A>
#d mailto#2 <a href="mailto: $1">$2</a>
#d news#1 <a href="news:$1">$1</a>
#d space &nbsp;
#d center#1 <center>$1</center>
#% #d center#1 <div class="center">$1</div>
#d newline <br>
#d h1#1 <h1>$1</h1>
#d h2#1 <h2>$1</h2>
#d h3#1 <h3>$1</h3>
#d h4#1 <h4>$1</h4>
#d h5#1 <h5>$1</h5>
#d img#2:3 <img src="$1" alt="$2" \optarg{$3}>
#d hline <hr>
#d begin#1:2 <$1 \optarg{$2}>
#d end#1 </$1>
#d bf#1:2 <strong \optarg{$2}>$1</strong>
#d sect#1 <h2><strong>$1</strong></h2>
#d subsect#1 <p><h3><strong>$1</strong></h3><p>
#d tt#1 <code>$1</code>

#d pagekeywords
#d pagedescription
#d meta#2 <meta name="$1" content="$2">
#d meta-description \meta{description}{\pagedescription}
#d meta-keywords \meta{keywords}{\pagekeywords}
#d meta-content-type <meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">

#% ------------------------------------------------------------------------
#% Macros for dealing with tables
#% ------------------------------------------------------------------------
#d table#3 <table width="$1" $2>$3</table>
#d tr#1 <tr>$1</tr>
#d td#3 <td width="$1" valign=top $2>$3</td>

#d stylesheet#1 <link rel="stylesheet" type="text/css" href="$1">
#d comment#1 <!-- $1 -->
