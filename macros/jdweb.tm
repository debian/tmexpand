#i html.tm
#i html_extra.tm

#d jdweb_start_content
    
#% code for the corner and top
#d pagename My Home Page
#d corner_html \space
#d banner_html \newline\center{\h1{\pagename}}

#% Next and previous links if required for multi-page document
#d next_node
#d prev_node

#% The user can customize these by redefining in sidebar.tm
#d sidebar_cellpadding 5
#d sidebar_width 10%
#d body_width 90%
#d sidebar_spacing 0%
#d sidebar_href#2 \href{$1}{$2}{class="sidebar"}

#d td_bg#3:4 \td{$1}{bgcolor="$2" \optarg{$4}}{$3}

#s+
#i jdweb_funs.sl
#s-
#d bgcolortheme \convert_color_to_theme{\sidebar_bg_color}

#% These are useful macros for user supplied sidebar.tm

#d sidebar_item#2 \tr{\td{100%}{class="sidebar" bgcolor="$1" align=right}{$2}}

#%+ 
   For strict 4.01 compliance, the bgcolor attribute should not be use.
   Instead use these:
     #d sidebar_item#2 \tr{\td{100%}{class="sidebar" align=right}{$2}}
     #d td_bg#3:4 \td{$1}{$4}{$3}
#%-

#d sidebar_link_active#2 \
 \sidebar_item{\sidebar_bg_color}{\
 \sidebar_href{$1}{$2}\space\graphic_hw{\tmdist_imagedir/sidebar_arrow_\bgcolortheme.png}{}{13}{12}}
#d sidebar_link_inactive#1 \sidebar_item{\sidebar_bg_color}{&gt;$1\space\space}
#d sidebar_text#1 \sidebar_item{\sidebar_bg_color}{$1}
#d sidebar_label#1 \sidebar_item{\sidebar_label_bg_color}{ \
   \bf{$1}{class="sidebarlabel"}}

#d sidebar_space \sidebar_item{\sidebar_bg_color}{\space}

#% The jdweb_start_html is a hook allowing one to display something
#% before the start of the table

#d jdweb_start_html
#d jdweb_end_html <hr>\p{class="jdwebend"} \lastupdated \emailme \
  <br>\checkhtml \madewithjed \anybrowser

#d jdweb_end \jdweb_end_html\body-end\html-end
#d sidebar_link#2 \
 \if{path_sans_extname(path_basename(__TM_FILE__))==path_sans_extname(path_basename("$1"))}\
  {\sidebar_link_inactive{$2}}{\sidebar_link_active{$1}{$2}}

#d baseurl

#% --------------------------------------------------------------------
#% Here is where the document starts
#% --------------------------------------------------------------------
\html-start
\head{\title{\pagename}
\ifarg{\baseurl}{\base{\baseurl}}
#i jdwebstyle.tm
#% \stylesheet{\stylesheet_url}
}
 
\body{bgcolor="\bg_color"}

\jdweb_start_html

\table{100%}{border=0 cellspacing=0 cellpadding=0 summary="\pagename"}{
#% corner and banner row
\tr{
  \td_bg{\sidebar_width}{\corner_bg_color}{\corner_html}{class="corner"}
  \td_bg{\sidebar_spacing}{\top_bg_color}{\space}{class="top"}
  \td_bg{\body_width}{\top_bg_color}{\banner_html}{class="top"}}

#% Now the sidebar and body row

\tr{
\td_bg{\sidebar_width}{\sidebar_bg_color}{
  \table{100%}{class="sidebar" cellpadding=\sidebar_cellpadding summary="sidebar menu"}{
#i sidebar.tm
  }}

\td_bg{\sidebar_spacing}{\bg_color}{\space}
\td_bg{\body_width}{\bg_color}{

\newline
\p 

#% Main body goes here.
\jdweb_start_content

