#i docbook.tm
#d manpage#3 \docbook{refentry} \
  <refentry id="$1">\__newline__\
  <refmeta><refentrytitle>$1</refentrytitle>\__newline__\
  <manvolnum>$2</manvolnum>\__newline__\
  </refmeta>\__newline__\
  <refnamediv><refname>$1</refname>\__newline__\
  <refpurpose>$3</refpurpose>\__newline__\
  </refnamediv>
#d manpage-end </refentry>

#d manarg_req#1 <arg choice="req">$1</arg>
#d manarg_rep#1 <arg choice="opt" rep="repeat">$1</arg>
#d manarg_plain#1 <arg choice="plain">$1</arg>

#d mansynopsis#2 \
  <refsynopsisdiv>\__newline__\
    <cmdsynopsis>\__newline__<command>$1</command>\__newline__\
      $2\__newline__\
    </cmdsynopsis>\__newline__\
  </refsynopsisdiv>

#d refsect1#1 <refsect1><title>$1</title>
#d refsect1-end </refsect1>
