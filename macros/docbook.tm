#i tmutil.tm
#% #d __verbatim_start <screen width="80">
#% #d __verbatim_end </screen>

#d __verbatim_begin <programlisting>
#d __verbatim_end </programlisting>

#d docbook_tag_with_attribute#3 \
  \if{\strlen{$3}}{<$1 $2>$3}{<$1>$2}
#d docbook_tag_with_attribute_end#3 \
  \docbook_tag_with_attribute{$1}{$2}{$3}</$1>

#%
#d docbook#1 <!DOCTYPE $1 PUBLIC "-//OASIS//DTD DocBook V3.1//EN">
#d book#0:1 <book \optarg{$1}>
#d book-end </book>
#d article#0:1 <article \optarg{$1}>
#d article-end </article>
#d title#1 <title>$1<title>

#d para#1 <para>$1<para>
#d p <para>
#d pp </para><para>
#d p-end </para>

#d artheader#1 <artheader>$1<artheader>
#d author#1 <author>$1</author>
#d firstname#1 <firstname>$1</firstname>
#d othername#1 <othername>$1</othername>
#d affiliation#1 <affiliation>$1</affiliation>
#d orgname#1 <orgname>$1</orgname>
#d orgdiv#1 <orgdiv>$1</orgdiv>
#d address#1 <address>$1</address>
#d email#1 <email>$1</email>
#d pubdate#1 <pubdate>$1</pubdate>

#d application#1 <application>$1</application>
#d emphasis#1 <emphasis>$1</emphasis>
#d itemizedlist#1 <itemizedlist>$1</itemizedlist>
#d listitem#1 <listitem>$1</listitem>

#d filename#1 <filename>$1</filename>
#d directory#1 <filename class="directory">$1</filename>

#d citetitle <citetitle>$1</citetitle>
#d ulink#2 <ulink url="$1">$2</ulink>
#d xref#1 <xref linkend="$1">

#d sect1#1 <sect1>$1</sect1>
#d sect2#1 <sect1>$1</sect1>

#d section#1:2 \if{\strlen{$2}{<sect1 id="$1">\title{$2}}{<sect1>\title{$1}}
#d subsection#1:2 \if{\strlen{$2}{<sect2 id="$1">\title{$2}}{<sect2>\title{$1}}
#d end-section </sect1>
#d end-subsection </sect2>

#d arg#1:2 \docbook_tag_with_attribute_end{arg}{$1}{$2}

#d replaceable#1 <replaceable>$1</replaceable>
#d option#1:2 <option>$1\ifarg{$2}{ \replaceable{$2}}</option>

#d group#1 <group>$1</group>
#d guimenu <guimenu>$1</guimenu>

#% Lists
#d variablelist#0:1 <variablelist>\ifarg{$1}{\title{$1}}
#d variablelist-end </variablelist>

#d varlistentry#2 <varlistentry>$1\__newline__\
    <listitem>$2</listitem>\__newline__</varlistentry>

#d command#1 <command>$1</command>
#d literal#1 <literal>$1</literal>

#d simplelist <simplelist>\__newline__
#d member#1 <member>$1</member>\__newline__
#d simplelist-end </simplelist>\__newline__

#d term#1 <term>$1</term>
