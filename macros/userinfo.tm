#% --------------------------------------------------------------------
#%   Each user should copy this file somewhere on the tm include path
#%   and edit it accordingly.  Defaults shown below.
#% --------------------------------------------------------------------
#d tmdist_imagedir /tmdist_images
#d hostname \get_hostname
#d email-address \get_username@\get_hostname
#d rooturl http://\hostname/~\get_username
#d myhomepage \rooturl/index.html
#d myname \get_realname

#s+
 vmessage ("**** WARNING: You are using the distribution version of userinfo.tm\n");
#s-
