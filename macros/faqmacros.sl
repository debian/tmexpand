static variable Faq_List_Root = NULL;
static variable Faq_List_Tail = NULL;
static variable Num_Faqs = 0;

static define faq_add_faq (q, a)
{
   variable s = struct 
     {
	q, a, next
     };
   s.q = q;
   s.a = a;
   s.next = NULL;

   if (Faq_List_Tail != NULL)
     Faq_List_Tail.next = s;
   else
     Faq_List_Root = s;
   
   Faq_List_Tail = s;
   Num_Faqs++;
}

static define faq_insert_questions ()
{
   variable s = Faq_List_Root;
   variable n = 1;
   while (s != NULL)
     {
	vinsert ("\\faq-quest-question{%d}{%s}\n", n, s.q);
	s = s.next;
	n++;
     }
}

static define faq_insert_answers ()
{
   variable s = Faq_List_Root;
   variable n = 1;
   while (s != NULL)
     {
	vinsert ("\\faq-ans-question{%d}{%s}\n", n, s.q);
	vinsert ("\\faq-ans-answer{%d}{%s}\n", n, s.a);
	s = s.next;
	n++;
     }
}

tm_add_macro ("faq_add_faq",&faq_add_faq, 2, 2);
tm_add_macro ("faq_insert_questions",&faq_insert_questions, 0, 0);
tm_add_macro ("faq_insert_answers",&faq_insert_answers, 0, 0);
