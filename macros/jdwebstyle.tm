#%+
 This stylesheet is designed to work with the jdweb macros.  For
 simplicity, the frame is divided into 6 sections:
 
       <----------------- start(user defined) --------------->
       <-- corner---> < ----------- top --------------------->
       <-- sidebar--> < ----------- body -------------------->
       <------------------- end ----------------------------->
       
 Classes with corresponding names will be used to
 customized objects.
 So, here is the stylesheet:
#%-
\begin{style type="text/css"}\comment{
  \jdweb_start_style
  \corner_style
  \top_style
  \sidebar_style
  \body_style
  \jdweb_end_style
}\end{style}
#%+
 Note also that the body_style will be used as the default style for
 everything not explicitly given a style.  It is also wrapped in a
 comment to prevent it from confusing non-stylesheet aware browsers.
#%-

#%+
 The stylesheet will consist of a number of entries.  A few generic
 macros macro will facilitate their construction.
#%-
#d style_entry#2 $1 \{ $2 \}\__newline__
#d style_fgbg_entry#3:4 \style_entry{$1}{color: $2; background-color: $3; \optarg{$4}}
#d style_font_entry#3 \style_entry{$1}{font-family: $2; font-size: $3;}
#%+
 Setup the body style first.  Since it is the default, I will leave
 off the body_ prefix for its attributes.  There is not a lot to do
 here, except for the colors and fonts.
#%-
#d fg_color black
#d bg_color white
#d link_fg_color #0000FF
#d link_bg_color \bg_color
#d visited_fg_color purple
#d visited_bg_color \link_bg_color
#d active_fg_color \rgb{255}{0}{102}
#d active_bg_color \bg_color
#d hover_fg_color \rgb{0}{153}{255}
#d hover_bg_color \bg_color
#d h1_fontsize 100%
#d h2_fontsize 100%
#d h3_fontsize 100%
#d h4_fontsize 100%
#d h5_fontsize 100%
#d pre_fontsize 100%
#d h1_fontfamily sans-serif
#d h2_fontfamily sans-serif
#d h3_fontfamily sans-serif
#d h4_fontfamily sans-serif
#d h5_fontfamily sans-serif
#d pre_fontfamily monospace
#d h1_style \style_font_entry{h1}{\h1_fontfamily}{\h1_fontsize}
#d h2_style \style_font_entry{h2}{\h2_fontfamily}{\h2_fontsize}
#d h3_style \style_font_entry{h3}{\h3_fontfamily}{\h3_fontsize}
#d h4_style \style_font_entry{h4}{\h4_fontfamily}{\h4_fontsize}
#d h5_style \style_font_entry{h5}{\h5_fontfamily}{\h5_fontsize}
#d pre_style \style_font_entry{pre}{\pre_fontfamily}{\pre_fontsize}

#% Put it all together to define the body style
#d body_style \
   \style_fgbg_entry{body}{\fg_color}{\bg_color} \
   \style_fgbg_entry{:link}{\link_fg_color}{\link_bg_color} \
   \style_fgbg_entry{:visited}{\visited_fg_color}{\visited_bg_color} \
   \style_fgbg_entry{:active}{\active_fg_color}{\active_bg_color} \
   \style_fgbg_entry{:visited:hover}{\hover_fg_color}{\hover_bg_color} \
   \style_fgbg_entry{:link:hover}{\hover_fg_color}{\hover_bg_color} \
   \h1_style \h2_style \h3_style \h4_style \h5_style \pre_style \
   \style_entry{div.center}{text-align: center;}

#%+
   Now the sidebar.  Unfortunately, I have not been able to get the
   pseudo-classes for the links to work with netscape 4 if I declare a 
   generic sidebar entry.  Also, the space between the sidebar table 
   cells do not get filled in unless I put the background tag in the 
   table element itself.  Netscape 6 does not have these problems.
#%-
#d sidebar_fg_color black
#d sidebar_bg_color #CCCCCC
#d sidebar_link_fg_color \rgb{0}{0}{153}
#d sidebar_link_bg_color \sidebar_bg_color
#d sidebar_visited_fg_color \sidebar_link_fg_color
#d sidebar_visited_bg_color \sidebar_bg_color
#d sidebar_active_fg_color \rgb{255}{0}{102}
#d sidebar_active_bg_color \sidebar_bg_color
#d sidebar_hover_fg_color \rgb{0}{153}{255}
#d sidebar_hover_bg_color \sidebar_bg_color
#d sidebar_label_font_family sans-serif
#d sidebar_label_fontsize 100%
#d sidebar_label_fg_color \sidebar_fg_color
#d sidebar_label_bg_color \sidebar_bg_color

#d sidebar_style \
  \style_fgbg_entry{.sidebar:link}{\sidebar_link_fg_color}{\sidebar_link_bg_color} \
   \style_fgbg_entry{.sidebar:visited}{\sidebar_visited_fg_color}{\sidebar_visited_bg_color} \
   \style_fgbg_entry{.sidebar:active}{\sidebar_active_fg_color}{\sidebar_active_bg_color} \
   \style_fgbg_entry{.sidebar:visited:hover}{\sidebar_hover_fg_color}{\sidebar_hover_bg_color} \
   \style_fgbg_entry{.sidebar:link:hover}{\sidebar_hover_fg_color}{\sidebar_hover_bg_color} \
   \style_entry{a.sidebar}{text-decoration: none;} \
   \style_font_entry{.sidebarlabel}{\sidebar_label_font_family}{\sidebar_label_fontsize} \
   \style_fgbg_entry{.sidebarlabel}{\sidebar_label_fg_color}{\sidebar_label_bg_color} \
   \style_fgbg_entry{td.sidebar,table.sidebar}{\sidebar_fg_color}{\sidebar_bg_color} \

#d corner_fg_color \sidebar_fg_color
#d corner_bg_color \sidebar_bg_color
#d corner_link_fg_color \rgb{0}{0}{153}
#d corner_link_bg_color \corner_bg_color
#d corner_visited_fg_color \corner_link_fg_color
#d corner_visited_bg_color \corner_bg_color
#d corner_active_fg_color \rgb{255}{0}{102}
#d corner_active_bg_color \corner_bg_color
#d corner_hover_fg_color \rgb{0}{153}{255}
#d corner_hover_bg_color \corner_bg_color
#d corner_style \
  \style_fgbg_entry{td.corner}{\corner_fg_color}{\corner_link_bg_color} \
  \style_fgbg_entry{.corner:link}{\corner_link_fg_color}{\corner_link_bg_color} \
   \style_fgbg_entry{.corner:visited}{\corner_visited_fg_color}{\corner_visited_bg_color} \
   \style_fgbg_entry{.corner:active}{\corner_active_fg_color}{\corner_active_bg_color} \
   \style_fgbg_entry{.corner:visited:hover}{\corner_hover_fg_color}{\corner_hover_bg_color} \
   \style_fgbg_entry{.corner:link:hover}{\corner_hover_fg_color}{\corner_hover_bg_color} \
   \style_entry{a.corner}{text-decoration: none;}

#d top_fg_color \sidebar_fg_color
#d top_bg_color \sidebar_bg_color
#d top_link_fg_color \rgb{0}{0}{153}
#d top_link_bg_color \top_bg_color
#d top_visited_fg_color \top_link_fg_color
#d top_visited_bg_color \top_bg_color
#d top_active_fg_color \rgb{255}{0}{102}
#d top_active_bg_color \top_bg_color
#d top_hover_fg_color \rgb{0}{153}{255}
#d top_hover_bg_color \top_bg_color
#d top_style \
  \style_fgbg_entry{td.top}{\top_fg_color}{\top_link_bg_color} \
  \style_fgbg_entry{.top:link}{\top_link_fg_color}{\top_link_bg_color} \
   \style_fgbg_entry{.top:visited}{\top_visited_fg_color}{\top_visited_bg_color} \
   \style_fgbg_entry{.top:active}{\top_active_fg_color}{\top_active_bg_color} \
   \style_fgbg_entry{.top:visited:hover}{\top_hover_fg_color}{\top_hover_bg_color} \
   \style_fgbg_entry{.top:link:hover}{\top_hover_fg_color}{\top_hover_bg_color} \
   \style_entry{a.top}{text-decoration: none;}

#%+
       User defined top
#%-
#d jdweb_start_style
#%+ 
           End
#%-
#d jdweb_end_style \
    \style_entry{p.jdwebend}{margin-top: 0em;}
