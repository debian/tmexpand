
\if{(\strlen{\prev_node})or(\strlen{\next_node})}{
  \p
  \table{100%}{border=0}{
  \tr{
  \td{50%}{align=left}{
    \if{\strlen{\prev_node}}
      {\bf{Previous:} \prev_node}
  }
  \td{50%}{align=right}{
    \if{\strlen{\next_node}}
      {\bf{Next:} \next_node}
  }
  }}
  \p
}

}}}

\jdweb_end

