#% Some standard macros
#i userinfo.tm

#d localurl#1 \rooturl/$1
#d graphic#2:3 \img{$1}{$2}{border=0 $3\__btrim__}
#d graphic_hw#4 \graphic{$1}{$2}{height=$3 width=$4}

#d emailme To comment on it or the material presented here, send \
   email to \email-address.

#d lastupdated This page was last updated \__today__ \
  by \href{\myhomepage}{\myname}.

#d checkhtml-url http://validator.w3.org/check/referer
#d validhtml_image \tmdist_imagedir/valid-html401.png
#d checkhtml \href{\checkhtml-url}{\graphic_hw{\validhtml_image}{Valid HTML 4.01!}{31}{88}}

#d jedsofturl http://www.jedsoft.org
#d jedhome \jedsofturl/jed/
#d madewithjed_image \tmdist_imagedir/madewithjed.jpg

#d madewithjed_url http://www.jedsoft.org/jed/madewithjed.html
#d madewithjed \href{\madewithjed_url}{\graphic_hw{\madewithjed_image}{Made with JED}{30}{90}}

#d anybrowser_image \tmdist_imagedir/anybrowser4b.png
#d anybrowser \href{http://www.anybrowser.org/campaign/}{\
    \graphic_hw{\anybrowser_image}{Viewable With Any Browser}{31}{88}}

#%  <br><p>This page is was \madewithjed to be \anybrowser, and is \
#%  \checkhtml compliant.

#% The \new macro displays a New! graphic.
#d new#1 \em{\graphic{\tmdist_imagedir/new.png}{$1}}\space{}

#s+
#i htmlmacros.sl
#s-
