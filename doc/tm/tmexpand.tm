#i docbook_man.tm

#d arg#1 <arg>$1</arg>\__newline__

#d tmexpand \command{tmexpand}
#d jed \command{jed}
#d jedscript \command{jed-script}
#d slang \literal{S-Lang}
#d ifun#1 \literal{$1}
#d sfun#1 \literal{$1}

\manpage{tmexpand}{1}{jed text-macro processor}

\mansynopsis{tmexpand}{
  \arg{\option{--version}}
  \arg{\option{-g}}
  \arg{\option{-I\replaceable{include-dir} \option{-I\replaceable{include-dir} ...}}}
  \arg{\replaceable{input-file}|-}
  \arg{\replaceable{output-file}|-}
}


\refsect1{DESCRIPTION}
  \p 
  \tmexpand is a simple \jedscript program for expanding a text-macro
  file.  Both the \slang and \jed documentation, as well as the
  jedsoft.org web pages are written in text-macro format.
  \p
  Included in the \tmexpand distribution are macros for the creation
  of jedsoft.org style HTML pages, macros for producing linuxdoc and
  docbook SGML files, and macros for expanding \slang function
  text-macro documentation to the \slang help file format.
  \p-end

\refsect1-end

#d man_options_entry#2 \varlistentry{\term{$1}}{\p $2 \p-end}\__newline__

\refsect1{OPTIONS}
    \variablelist
      \man_options_entry{\option{--version}}{
        Print version information
      }
      \man_options_entry{\option{-g}}{
        Load with _traceback set to 1.  This is useful for debugging
        \tmexpand itself.
      }
      \man_options_entry{\option{-I\replaceable{include-dir}}}{
        Append the directory to the macro-directory search list.
      }
    \variablelist-end
\refsect1-end

\refsect1{REQUIREMENTS}
\p
  \tmexpand is a \slang script that is processed by the \jed editor.
  Version 0.99-17 or later of the \jed editor is required to run the
  script.  To get the most out of \tmexpand, jed should be compiled
  against \slang version 2.

\refsect1-end

\refsect1{USAGE}
\p 
  This section is incomplete.
\p-end

\refsect1-end

\refsect1{BUGS}
\p
  Debugging the output when something goes wrong can be tricky.

\refsect1{AUTHOR}
\p
  The author of \tmexpand is John E. Davis <jed@jedsoft.org>.
\pp
  Permission is granted to copy, distribute and/or modify
  this document under the terms of the GNU General Public License,
  Version 2 any later version published by the Free Software
  Foundation.
\pp
  On Debian systems, the complete text of the GNU General Public
  License can be found in \filename{/usr/share/common-licenses/GPL}
\p-end
\refsect1-end

\manpage-end
