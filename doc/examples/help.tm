#%+
  Text appearing between the #%+/- delimiters will be ignored and not
  appear when expanded.
  
  This example shows how slang function documentation may be expanded
  using the slhlp macros.  It does not expand to .sgml, but to the
  text format that slang reads.
#%-
#i slhlp.tm
#d int Integer_Type

\function{my_function}
\synopsis{A function returning an integer}
\usage{\int my_function ()}
\description
  This function returns an integer.
\example
#v+
   if (my_function ()) exit (1);
#v-
\notes
  This function is obsolete
\seealso{my_other_function, bobs_function}
\done
